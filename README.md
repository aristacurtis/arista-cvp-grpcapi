# arista-cvp-grpcapi

Example Ansible modules for connecting to CVP GRPC resources; e.g. accessing Telemetry information from CVP using Ansible


# How To Use

## clone repository

```bash
$ git clone https://gitlab.com/aristacurtis/arista-cvp-grpcapi.git
$ cd arista-cvp-grpcapi
```

## modify relevant variables
## (NEED TO UPDATE DETAILS TO REFERENCE SERVICE ACCOUNT && AUTH TOKEN
  - inventory.ini | `change ansible_host, ansible_user, ansible_password variables`

## run it

```bash
## (NEED TO UPDATE DETAILS TO REFERENCE SERVICE ACCOUNT && AUTH TOKEN
ansible-playbook -i inventory.ini playbook.grpc.example
```
### what it does

1.  uses _cloudvision-python_ library to connect to CVP GRPC and collect telemetry data

## dependencies

This playbook requires the following to be installed on the Ansible control machine:

   -  python 3.7 (or later)
   -  ansible >= 2.9.0
   -  grpcio>=1.46.0
   -  msgpack>=1.0.3
   -  protobuf>=3.20.1
   -  typing-extensions>=4.2.0
   -  types-protobuf>=3.19.20
   -  types-PyYAML>=6.0.7
   -  types-requests>=2.27.25
   -  cloudvision>=1.3.0
      * can be installed with:
```bash 
$ pip3 install -r requirements.txt
``` 
   - **NOTE: if SELinux enabled, uncomment selinux line from requirements.txt or install individually:
```bash
$ pip3 install selinux
```
